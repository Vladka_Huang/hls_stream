package hls

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var mediaRoot = "assets/media/"
var ErrFileNotFound = errors.New("file not found")
var ErrDuplicateFileName = errors.New("duplicate file name under this dir")

func IndexPage(response http.ResponseWriter, request *http.Request) {
	http.ServeFile(response, request, "index.html")
}

func StreamHandler(response http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)

	topDir, ok := vars["topDir"]
	if !ok {
		response.WriteHeader(http.StatusNotFound)
		return
	}
	fileName, ok := vars["fileName"]
	if !ok {
		response.WriteHeader(http.StatusNotFound)
		return
	}

	m3u8Path, err := findM3u8Path(fileName, topDir)
	if err != nil {
		response.WriteHeader(http.StatusNotFound)
		response.Write(bytes.NewBufferString(err.Error()).Bytes())
		return
	}

	segName, ok := vars["segName"]
	if !ok {
		serveHlsM3u8(response, request, m3u8Path)
	} else {
		serveHlsTs(response, request, m3u8Path, segName)
	}
}

func findM3u8Path(fileName string, topDir string) (indexFile string, err error){
	m3u8Name := fileName + ".m3u8"
	params := []string{
		mediaRoot + topDir,
		"-name",
		m3u8Name,
	}
	cmd := exec.Command("find", params...)
	if fullPath, e := cmd.CombinedOutput(); e == nil && len(fullPath) != 0 {
		path := strings.TrimRight(string(fullPath), "\n")
		// 找到多個檔案符合時，return error
		if _, er := os.Stat(path); os.IsNotExist(er) {
			err = ErrDuplicateFileName
		}
		indexFile = path
	} else {
		err = ErrFileNotFound
	}

	return
}

func serveHlsM3u8(response http.ResponseWriter, request *http.Request, m3u8Path string) {
	http.ServeFile(response, request, m3u8Path)
	response.Header().Set("Content-Type", "application/x-mpegURL")
}

func serveHlsTs(response http.ResponseWriter, request *http.Request, m3u8Path, segName string) {
	mediaBase := filepath.Dir(m3u8Path)
	mediaFile := fmt.Sprintf("%s/%s", mediaBase, segName)
	http.ServeFile(response, request, mediaFile)
	response.Header().Set("Content-Type", "video/MP2T")
}