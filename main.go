package main

import (
	"./queue"
	"./routes"
	"fmt"
	"github.com/spf13/viper"
	"net/http"
	"./ffmpeg"
)

func route() {
	r := routes.NewRouter()
	_ = http.ListenAndServe(":8080", r)
}

func main() {
	readConfig()
	queue.CreatePool()
	pool := ffmpeg.CreatePool(10)

	forever := make(chan bool)
	go func() {
		if err := queue.ListenPubSubChannel("mediaConvert", pool); err != nil {
			fmt.Println(err)
		}
	}()

	go func() {
		for i := 0; i < 2; i++ {
			go pool.ConvertToHls(pool.Buffer)
		}
	}()
	route()
	<-forever
}

func readConfig() {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.SetConfigType("json")
	if err := viper.ReadInConfig(); err != nil {
		panic(fmt.Errorf("Fatal error redisConfig file: %s \n", err))
	}
}