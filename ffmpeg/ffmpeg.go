package ffmpeg

import (
	"fmt"
	"github.com/spf13/viper"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
)

type MediaConfig struct {
	BasePath    string `mapstructure:"base_path"`
	HlsTime     int    `mapstructure:"hls_time"`
	HlsListSize int    `mapstructure:"hls_list_size"`
}

type Pool struct {
	Buffer chan string
}

func CreatePool(size int) *Pool {
	if size <= 0 {
		size = 1
	}
	return &Pool{
		Buffer: make(chan string, size),
	}
}

func (pool *Pool) ConvertToHls(pathChan chan string) {
	var mediaConfig MediaConfig

	if err := viper.UnmarshalKey("media", &mediaConfig); err != nil {
		panic("Unable to unmarshal mediaConfig")
	}

	for srcPath := range pathChan {

		fileName := filepath.Base(srcPath)
		srcPath = mediaConfig.BasePath + srcPath
		dstPath := filepath.Dir(srcPath) + "/hls/"

		if _, err := os.Stat(filepath.Dir(srcPath)); os.IsNotExist(err) {
			log.Println(err)
			return
		}

		if _, err := os.Stat(dstPath); os.IsNotExist(err) {
			if err = os.Mkdir(dstPath, 0755); err != nil {
				log.Println(err)
				return
			}
		}

		segmentFilename := fileName + "_%05d.ts"
		outputFilename := fileName + ".m3u8"

		params := []string{
			"-i",
			srcPath,
			"-hls_time",
			strconv.Itoa(mediaConfig.HlsTime),
			"-hls_list_size",
			strconv.Itoa(mediaConfig.HlsListSize),
			"-hls_segment_filename",
			dstPath + segmentFilename,
			dstPath + outputFilename,
		}

		cmd := exec.Command("ffmpeg", params...)
		out, err := cmd.CombinedOutput()
		if err != nil {
			log.Println(err)
		}
		fmt.Println(string(out))
	}
	return
}