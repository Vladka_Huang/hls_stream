package routes

import (
	"../hls"
	"github.com/gorilla/mux"
	"net/http"
)

type Route struct {
	Method string
	Pattern string
	Handler http.HandlerFunc
	Middleware mux.MiddlewareFunc
}

var routes []Route

func init() {
	register("GET", "/", hls.IndexPage, nil)
	register("GET", "/media/{topDir}/{fileName}/stream/", hls.StreamHandler, nil)
	register("GET", "/media/{topDir}/{fileName}/stream/{segName}", hls.StreamHandler, nil)
}

func NewRouter() *mux.Router {
	router := mux.NewRouter()
	for _,route := range routes {
		router.HandleFunc(route.Pattern, route.Handler).Methods(route.Method)

		if route.Middleware != nil {
			router.Use(route.Middleware)
		}
	}
	return router
}

func register(method string, pattern string, handler http.HandlerFunc, middleware mux.MiddlewareFunc) {
	routes = append(routes, Route{method, pattern, handler, middleware})
}
