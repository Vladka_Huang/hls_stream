package queue

import (
	"../ffmpeg"
	"github.com/gomodule/redigo/redis"
)

func ListenPubSubChannel(channelName string, pool *ffmpeg.Pool) error {
	redisConn := Pool.Get()
	defer redisConn.Close()

	psc := redis.PubSubConn{Conn: redisConn}
	if err := psc.Subscribe(channelName); err != nil {
		return err
	}

	for {
		switch v := psc.Receive().(type) {
		case error:
			return v
		case redis.Message:
			pool.Buffer <- string(v.Data)
			//fmt.Printf("goroutine Num: %d\n", runtime.NumGoroutine())
		}
	}
}